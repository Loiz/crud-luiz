import{ 
    submit,
    deleteManga,
    editManga
} from './js/crud.js';

window.submit = submit;
window.deleteManga = deleteManga;
window.editManga = editManga;