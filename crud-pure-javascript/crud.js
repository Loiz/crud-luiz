var indexRow = null

function submit(){
    var manga = readManga()
    if(indexRow === null){
        createManga(manga);
    }else{
        updateManga(manga);
    }

    indexRow = null
    clear();

}

function readManga(){
    var manga = {}
    manga["title"] = document.getElementById("title").value
    manga["genre"] = document.getElementById("genre").value
    manga["year"] = document.getElementById("year").value
    manga["status"] = document.querySelector('input[name="status"]:checked').value

    return manga;
    
}

function createManga(manga){
    var table = document.getElementById("table")
    var row = table.insertRow(0)
    var title = row.insertCell(0)
    var genre = row.insertCell(1)
    var year = row.insertCell(2)
    var status = row.insertCell(3)
    var button = row.insertCell(4)
    
    title.innerHTML = manga.title
    genre.innerHTML = manga.genre
    year.innerHTML = manga.year
    status.innerHTML = manga.status
    button.innerHTML = `<button onclick='editManga(this)'>Edit</button> <button onclick='deleteManga(this)'>Del</button>`
   
}

function editManga(rowManga){
    indexRow = rowManga.parentElement.parentElement;
    document.getElementById("title").value = indexRow.cells[0].innerHTML
    document.getElementById("genre").value = indexRow.cells[1].innerHTML
    document.getElementById("year").value = indexRow.cells[2].innerHTML 
    document.getElementById(indexRow.cells[3].innerHTML).checked = true

}

function updateManga({title,genre,year,status}){
    document.getElementById
    indexRow.cells[0].innerHTML = title
    indexRow.cells[1].innerHTML = genre
    indexRow.cells[2].innerHTML = year
    indexRow.cells[3].innerHTML = status

}

function deleteManga(rowManga){
    row = rowManga.parentElement.parentElement;
    var index = row.rowIndex - 1;
    document.getElementById('table').deleteRow(index);
}

function clear(){
    document.getElementById("title").value = ""
    document.getElementById("genre").value = ""
    document.getElementById("year").value = ""
    document.getElementById("publishing").checked = false
    document.getElementById("finished").checked = false;
}