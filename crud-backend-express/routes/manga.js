var express = require('express');
var router = express.Router();
var db = require("../db_manga/dbManga")
var Genres = require("../models/genres")
var erros = require("../erros")

router.get('/', async (req, res) => {
    const page = req.query.page
    const limit = req.query.limit

    const sIndex = (page - 1) * limit

    const mangas = await db('manga')
        .select('manga.*')
        .orderBy('id_manga', 'asc')
        .limit(limit)
        .offset(sIndex)

    res.json(mangas)

})

router.get('/genres', async (req, res) => {
    try {
        const genres = await Genres.find()

        res.send(genres)
    } catch (error) {
        res.status(400).send(error)

    }
})

router.post('/genres', async (req, res) => {
    try {
        const genre = await Genres.create(req.body);

        res.send({genre})
    } catch (error) {
        res.status(400).send(error)

    }

})

router.delete('/genres', async (req, res) => {
    try {
    
        await Genres.deleteOne(req.body.name)
        const genres = await Genres.find({})
        res.send(genres)

    } catch (error) {
        res.status(400).send(error)

    }
})


router.post('/', async (req, res) => {
    try {
        const { title, genres, released, status } = req.body
        const result = await db('manga')
            .returning('manga')
            .insert({ title, genres, released, status })

        res.json(result)

    } catch (error) {

        console.log(error);
        if (error.code == "23505") {
            res.status(400).send(erros["TITLE_UNIQUE"]);
        } else {
            res.status(400).send(error)
        }

    }
})

router.delete('/:id_manga', async (req, res) => {
    const id = req.params;
    const manga = await db('manga')
        .where(id)
        .del()

    res.json(manga)
})

router.put('/:id_manga', async (req, res) => {
    try {
        const id = req.params;
        const { title, genres, released, status } = req.body;
        const manga = await db('manga')
            .where(id)
            .update({
                title,
                genres,
                released,
                status
            })

        res.json(manga)
    } catch (error) {

        if (error.code == "23505") {
            res.status(400).send(erros["TITLE_UNIQUE"]);
        } else {
            res.status(400).send(error)
        }

    }

})


module.exports = router;