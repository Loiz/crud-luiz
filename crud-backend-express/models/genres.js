const mongoose = require('../db_manga/dbMongo')

const genresSchema = new mongoose.Schema({

    name: {
        type: String,
        require: true,
        unique: true
    }
})

const Genres = mongoose.model('genres', genresSchema)

module.exports = Genres 