const knex = require('knex')

module.exports = knex({
  client: 'postgres',
  connection: {
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'test_db',
  },
})